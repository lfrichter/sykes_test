<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    public $timestamps = false;

    public function location()
    {
        return $this->hasOne(Location::class,'__pk','_fk_location');
    }

    public function priceBands()
    {
        return $this->hasMany(PriceBand::class, '_fk_property', '__pk');
    }  

    public function bookings()
    {
        return $this->hasMany(Booking::class, '_fk_property', '__pk');
    }  
}
