<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Assesment Sykes - Search</title>

        <!-- Fonts -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    </head>
    <body>

        <div class="jumbotron">
            <div class="container">
                <h2 class="pull-right">Assesment Sykes</h2>
                <h2>Search</h2>

                <div class="row">
                    <div class="col-12">

                        <!-- Search form -->
                        <form action="{{ route('search') }}" class="form-inline" method="GET">
                            @csrf
                            <i class="fas fa-search" aria-hidden="true"></i>
                            <div class="btn-group mr-0 pr-0" role="group" aria-label="Search">
                                <input class="form-control form-control-sm ml-3 w-75" type="text" name="search" placeholder="Search" aria-label="Search" @if(isset($search)) value="{{ $search }}" @endif>
                            </div>

                             <div class="form-check">
                                <input type="checkbox" class="form-check-input" name="near_beach" id="near_beach" value="1" @if(isset($near_beach)) checked="cheked" @endif>
                                <label class="form-check-label" for="near_beach">Near the beach</label>
                            </div>

                             <div class="form-check ml-4">
                                <input type="checkbox" class="form-check-input" name="accepts_pets" id="accepts_pets" value="1" @if(isset($accepts_pets)) checked="cheked" @endif>
                                <label class="form-check-label" for="accepts_pets">Accepts pets</label>
                            </div>

                            <div class="form-group ml-4">
                                <label for="sleeps">Sleeps</label>
                                <select class="form-control form-control-sm ml-2" name="sleeps" id="sleeps">
                                    <option value="">-</option>
                                    @foreach($sleeps_all as $sleep_)
                                        <option value="{{ $sleep_ }}" @if(isset($sleeps) and $sleeps == $sleep_) selected="selected" @endif>{{ $sleep_ }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group ml-4">
                                <label for="beds">Beds</label>
                                <select class="form-control form-control-sm ml-2" name="beds" id="beds">
                                    <option value="">-</option>
                                    @foreach($beds_all as $bed_)
                                        <option value="{{ $bed_ }}" @if(isset($beds) and $beds == $bed_) selected="selected" @endif>{{ $bed_ }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group ml-4">
                                <label for="date_in">Arriving</label>
                                <input type="text" value="@if(isset($start_date)) {{ $start_date }} @endif" id="start_date" name="start_date" class="form-control form-control-sm ml-3 w-60">
                            </div>

                            <div class="form-group ml-4">
                                <label for="nights">Nights</label>
                                <select class="form-control form-control-sm ml-2" name="nights" id="nights">
                                    @for ($i = 1; $i < 10; $i++)
                                        <option value="{{ $i }}" @if(isset($nights) and $nights == $i) selected="selected" @endif>{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>

                            <div class="form-group ml-4 mt-4">
                                <button type="submit" class="btn-sm btn-primary ml-2">Do search</button>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>

        <div class="container">

            @if(isset($properties))

                <div class="row">
                    <div class="col-12 mt-4">
                        
                        <h3>Properties</h3>

                        @if($properties->count() == 0)

                            <small class="text-danger">No results found</small>

                        @else

                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Location</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($properties as $property)
                                    <tr>
                                        <td>{{ $property->property_name }}</td>
                                        <td>{{ $property->location->location_name }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            {{ $properties->links() }}

                        @endif

                    </div>
                </div>

            @endif

        </div>

        <script src="{{ asset('js/app.js') }}"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $(document).ready( function() {
                $("#start_date").datepicker();
            });
        </script>
    </body>
</html>

