<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PriceBand extends Model
{
    public $timestamps = false;

    public function property()
    {
        return $this->hasOne(Property::class,'__pk','_fk_property');
    }
}
