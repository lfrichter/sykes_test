<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Property;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    public function search(Request $request){

        $per_page = 2;
        $checkboxes = ['near_beach','accepts_pets'];
        $dropboxes = ['sleeps','beds'];
        $queries = [];

        // dd( $request->all() );

        $properties = new Property;

        // main search by string
        if( $request->has('search') ){
            $search = $request->search;
            $properties = $properties->where('property_name','like','%' . $search  . '%');
            $queries['search'] = $search;
        }

        // checkboxes filters
        foreach($checkboxes as $column){
            if( $request->has($column) ){
                $properties = $properties->where($column,request($column));
                $queries[$column] = request($column);
            }
        }

        // dropboxes filters
        foreach($dropboxes as $column){
            if( $request->has($column) and !empty($request->$column)){
                $properties = $properties->where($column,'>=',request($column));
                $queries[$column] = request($column);
            }
        }

            // start date and nights filter
        if( $request->has('start_date') and !is_null($request->start_date) ){
            
            $arr = explode('/',$request->start_date);
            $start_date = Carbon::create($arr[2], $arr[0], $arr[1], 0, 0, 0, 'Europe/London');
            $end_date = $start_date->copy()->addDay($request->nights);

            $start_date_ = $start_date->format('m/d/Y');

            // dd($start_date_);

            $start_date  = $start_date->format('Y-m-d');
            $end_date    = $end_date->format('Y-m-d');
            
            $properties = $properties->whereDoesntHave('bookings', function ($query) use($start_date, $end_date) {
                //1. greater than today   
                // $query->where('start_date', '>', date('Y-m-d'));
                //2. less than start date...
                $query->whereBetween('start_date', [$start_date, $end_date]);
                    //->orWhereBetween('end_date', [$start_date, $end_date]);
            });

            // dd($start_date, $end_date, $properties->get() );

            // $properties = $properties->whereDoesntHave('bookings', function ($query) use($request) {
            //     //1. greater than today   
            //     $query->where('start_date', '>', date('Y-m-d'));
            //     //2. less than start date...
            //     $query->where('start_date', '>', $request->start_date);
            // });
                // Reservation::whereBetween('reservation_from', [$from1, $to1])
                // ->orWhereBetween('reservation_to', [$from2, $to2])
                // ->whereNotBetween('reservation_to', [$from3, $to3])
                // ->get();
            // dd($properties);
            $queries['start_date'] = $start_date_; 
            // dd( $queries['start_date'] );
            $queries['nights'] = $request->nights;
        }


         // pagination
        $properties = $properties->paginate($per_page)->appends($queries);

        // get dropboxes values
        $sleeps_all['sleeps_all'] = Property::select('sleeps')->orderBy('sleeps')->get()->pluck('sleeps')->toArray();
        $beds_all['beds_all'] = Property::select('beds')->orderBy('beds')->get()->pluck('beds')->toArray();

        // deliver all params
        $params = compact('properties');
        $params = array_merge($params, $queries, $sleeps_all, $beds_all);

        return view('index', $params);
    }


}
